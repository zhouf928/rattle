Package: rattle
Type: Package
Title: Graphical User Interface for Data Science in R
Version: 5.0.19
Date: 2017-09-04
Authors@R: c(person("Graham", "Williams", 
	   	    role=c("aut", "cph", "cre"),
	            email="Graham.Williams@togaware.com"),
	     person("Mark", "Vere Culp", role="cph"),
             person("Ed", "Cox", role="ctb"), 
	     person("Anthony", "Nolan", role="ctb"),
	     person("Denis", "White", role="cph"),
	     person("Daniele", "Medri", role="ctb"),
	     person("Akbar", "Waljee", role="ctb", 
	     	    comment="OOB AUC for Random Forest"),
             person("Brian", "Ripley", role="cph",
	            comment="print.summary.nnet"),
	     person("Jose", "Magana", role="ctb",
                    comment="ggpairs plots"),
             person("Surendra", "Tipparaju", role="ctb",
	            comment="initial RevoScaleR/XDF"),
             person("Durga", "Prasad Chappidi", role="ctb",
	            comment="initial RevoScaleR/XDF"),
             person("Dinesh", "Manyam Venkata", role="ctb",
	            comment="initial RevoScaleR/XDF"),
             person("Mrinal", "Chakraborty", role="ctb",
	            comment="initial RevoScaleR/XDF"),
             person("Fang", "Zhou", role="ctb",
	            comment="initial xgboost"))
Depends: R (>= 2.13.0)
Imports: stats, utils, ggplot2, grDevices, graphics, magrittr, methods,
  RGtk2, stringi, stringr, tidyr, dplyr, cairoDevice, XML, rpart.plot
Suggests: pmml (>= 1.2.13), bitops, colorspace, 
  ada, amap, arules, arulesViz, biclust,
  cba, cluster, corrplot, descr, doBy,
  e1071, ellipse, fBasics, foreign, fpc, 
  gdata, ggdendro, ggraptR, gplots, grid, 
  gridExtra, gtools, gWidgetsRGtk2, 
  hmeasure, Hmisc, kernlab, Matrix, mice,
  nnet, odfWeave, party, playwith, plyr, psych, 
  randomForest, rattle.data, RColorBrewer, readxl, reshape, rggobi,
  ROCR, RODBC, rpart,
  SnowballC, survival, timeDate, tm, verification, 
  wskm, RGtk2Extras, xgboost
Description: The R Analytic Tool To Learn Easily (Rattle) provides a 
  Gnome (RGtk2) based interface to R functionality for data science. 
  The aim is to provide a simple and intuitive interface 
  that allows a user to quickly load data from a CSV file 
  (or via ODBC), transform and explore the data, 
  build and evaluate models, and export models as PMML (predictive
  modelling markup language) or as scores. All of this with knowing little 
  about R. All R commands are logged and commented through the log tab. Thus they
  are available to the user as a script file or as an aide for the user to 
  learn R or to copy-and-paste directly into R itself. 
  Rattle also exports a number of utility 
  functions and the graphical user interface, invoked as rattle(),  does 
  not need to be run to deploy these.  
License: GPL (>= 2)
LazyLoad: yes
LazyData: yes
URL: http://rattle.togaware.com/
